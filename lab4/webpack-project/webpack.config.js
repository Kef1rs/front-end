const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    entry: {
        main: path.resolve(__dirname, './src/index.js'),
    },
    output: {
        path: path.resolve(__dirname, './disto'),
        filename: '[name].bundle.js',
    },
    plugins: [
        new CleanWebpackPlugin({
            cleanAfterEveryBuildPatterns: ['disto']
        }),
        new HtmlWebpackPlugin({
        title: 'Моя страница',
        template: path.resolve(__dirname, './src/pages/index.html'),
        filename: 'index.html',
        }),
        new HtmlWebpackPlugin({
            title: 'Новости',
            template: path.resolve(__dirname, './src/pages/news.html'),
            filename: 'news.html',
        }),
        new HtmlWebpackPlugin({
            title: 'Фото',
            template: path.resolve(__dirname, './src/pages/photo.html'),
            filename: 'photo.html',
        }),
        new HtmlWebpackPlugin({
            title: 'Расписание',
            template: path.resolve(__dirname, './src/pages/rozklad.html'),
            filename: 'rozklad.html',
        })
    ],
    devServer: {
        contentBase: './src/pages',
        port: 7700,
    } 
}
